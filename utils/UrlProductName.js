const proNameToUrl = (proName) => {
  return proName.toLowerCase().split(" ").join("-");
};

const urlToProName = (url) => {
  return url
    .split("-").join(" ")
    .toLowerCase()
    .split(" ")
    .map((s) => s.charAt(0).toUpperCase() + s.substring(1))
    .join(" ");
};

export { proNameToUrl, urlToProName };

import { PageSizeTable, PageSizeCard } from "./PageSize";
import defaultProName from "./defaultProName";

export { PageSizeCard, PageSizeTable, defaultProName };

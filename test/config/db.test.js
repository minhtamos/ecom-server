import chai from "chai";
import mocha from "mocha";
import { connectDb, closeDb, clearDb } from "../../config/db";

const describe = mocha.describe;

describe("Test connect to db", () => {
  it("Connected to db when test", (done) => {
    connectDb("test")
      .then(() => {
        done();
      })
      .catch((err) => {
        done(err);
      });
  });
  it("Connected to db when dev", (done) => {
    connectDb("dev")
      .then(() => {
        done();
      })
      .catch((err) => {
        done(err);
      });
  });
});

describe("Test close connect to db", () => {
  it("Closed connect to db", (done) => {
    closeDb()
      .then(() => {
        done();
      })
      .catch((err) => {
        done(err);
      });
  });
});

import chai from "chai";
import mocha from "mocha";
import { urlToProName, proNameToUrl } from "../../utils";

const expect = chai.expect;
const describe = mocha.describe;

describe("Test util UrlProductName", () => {
  describe("proNameToUrl function", () => {
    it("proNameToUrl function works correctly", () => {
      const url = proNameToUrl("one two three");
      expect(url).equal("one-two-three");
    });
  });
  describe("proNameToUrl function", () => {
    it("urlToProName function works correctly", () => {
      const url = urlToProName("one-two-three");
      expect(url).equal("One Two Three");
    });
  });
});

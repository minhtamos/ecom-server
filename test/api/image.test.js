import request from "supertest";
import chai from "chai";
import mocha from "mocha";
import server from "../../server";
import path from "path";

const describe = mocha.describe;
const expect = chai.expect;

describe("Image API /api/image/", () => {
  let imgPath = "";
  it("Post an image", (done) => {
    request(server)
      .post(`/api/image`)
      .attach("image", path.resolve(__dirname, "../images/lena.jpg"))
      .then((res) => {
        const { success, url } = res.body;
        expect(success).equal(true);
        expect(url).contain(`public/images/`);
        imgPath = url;
        done();
      })
      .catch((error) => {
        done(error);
      });
  });
  it("Delete an image", (done) => {
    const name = imgPath.split("/")[3];
    request(server)
      .delete(`/api/image?name=${name}`)
      .then((res) => {
        const { success, name } = res.body;
        expect(success).equal(true);
        expect(name).equal(name);
        done();
      });
  });
});

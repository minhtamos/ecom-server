import request from "supertest";
import chai from "chai";
import mocha from "mocha";
import server from "../../server";

const describe = mocha.describe;
const expect = chai.expect;
const name = "name";
let id = "";

describe("POST /api/category", () => {
  it("Post a category", (done) => {
    request(server)
      .post("/api/category")
      .send({ name: name })
      .then((res) => {
        const { success, category } = res.body;
        expect(success).equal(true);
        expect(category.name).equal(name);
        id = category._id;
        done();
      })
      .catch((error) => {
        done(error);
      });
  });
  it("Post a category with existed name", (done) => {
    request(server)
      .post("/api/category")
      .send({ name: name })
      .then((res) => {
        const { success, error } = res.body;
        expect(success).equal(false);
        expect(error.name).equal("MongoError");
        expect(error.errmsg).equal(
          'E11000 duplicate key error dup key: { : "name" }'
        );
        done();
      })
      .catch((error) => {
        done(error);
      });
  });
  it("Post a category with no name", (done) => {
    request(server)
      .post("/api/category")
      .send({})
      .then((res) => {
        const { success, error } = res.body;
        expect(success).equal(false);
        expect(error.name).equal("ValidationError");
        expect(error.message).equal(
          "Category validation failed: name: Path `name` is required."
        );
        done();
      })
      .catch((error) => {
        done(error);
      });
  });
});

describe("GET /api/category", () => {
  it("Get all categories works", (done) => {
    request(server)
      .get("/api/category")
      .then((res) => {
        const { success, categories } = res.body;
        expect(success).equal(true);
        expect(Array.isArray(categories)).equal(true);
        expect(categories[0].name).equal(name);
        done();
      })
      .catch((error) => {
        done(error);
      });
  });
});

import request from "supertest";
import chai from "chai";
import mocha from "mocha";
import server from "../../server";

const describe = mocha.describe;
const expect = chai.expect;
const name = "name";
const email = "email@gmail.com";
const password = "pass";

describe("POST /api/user/", () => {
  it("Normal register /api/user/register", (done) => {
    request(server)
      .post("/api/user/register")
      .send({ name, email, password })
      .then((res) => {
        const { success, user } = res.body;
        expect(success).equal(true);
        expect(user.email).equal(email);
        expect(user.name).equal(name);
        expect(user.password).equal(password);
        expect(user.role).equal("user");
        done();
      })
      .catch((err) => done(err));
  });
  it("Register existed email /api/user/register", (done) => {
    request(server)
      .post("/api/user/register")
      .send({ name, email, password })
      .then((res) => {
        const { success, error } = res.body;
        expect(success).equal(false);
        expect(error.name).equal("user existed");
        done();
      })
      .catch((err) => done(err));
  });
  it("Register nothing /api/user/register", (done) => {
    request(server)
      .post("/api/user/register")
      .send({})
      .then((res) => {
        const { success, error } = res.body;
        expect(success).equal(false);
        expect(error.name).equal("ValidationError");
        done();
      })
      .catch((err) => done(err));
  });
  it("Normal login /api/user/login", (done) => {
    request(server)
      .post("/api/user/login")
      .send({ email, password })
      .then((res) => {
        const { success, user } = res.body;
        expect(success).equal(true);
        expect(user.email).equal(email);
        expect(user.password).equal(password);
        done();
      });
  });
  it("Wrong email pass login", (done) => {
    request(server)
      .post("/api/user/login")
      .send({ email, password: password + "changed" })
      .then((res) => {
        const { success, error } = res.body;
        expect(success).equal(false);
        expect(error.name).equal("wrong email or password");
        done();
      });
  });
  it("Change email name /api/user/info", (done) => {
    request(server)
      .put("/api/user/info")
      .send({
        email: email + "changed",
        name: name + "changed",
        oldEmail: email,
      })
      .then((res) => {
        const { success, user } = res.body;
        expect(success).equal(true);
        expect(user.email).equal(email + "changed");
        expect(user.name).equal(name + "changed");
        done();
      });
  });
  it("Change email name with no user found /api/user/info", (done) => {
    request(server)
      .put("/api/user/info")
      .send({
        email: email + "changed",
        name: name + "changed",
        oldEmail: email + "changed1",
      })
      .then((res) => {
        const { success, error } = res.body;
        expect(success).equal(false);
        expect(error.name).equal("User not found");
        done();
      });
  });
  it("Change email name with name undefined /api/user/info", (done) => {
    request(server)
      .put("/api/user/info")
      .send({
        email: email + "changed",
        oldEmail: email + "changed",
      })
      .then((res) => {
        const { success, error } = res.body;
        expect(success).equal(false);
        expect(error.name).equal("ValidationError");
        expect(error.message).equal(
          "User validation failed: name: Path `name` is required."
        );
        done();
      });
  });
  it("Change password works /api/user/password", (done) => {
    request(server)
      .put("/api/user/password")
      .send({
        email: email + "changed",
        curPass: password,
        newPass: password + "changed",
      })
      .then((res) => {
        const { success, user } = res.body;
        expect(success).equal(true);
        expect(user.email).equal(email + "changed");
        expect(user.password).equal(password + "changed");
        done();
      });
  });
  it("Change password with wrong current password /api/user/password", (done) => {
    request(server)
      .put("/api/user/password")
      .send({
        email: email + "changed",
        curPass: password + "changed1",
        newPass: password + "changed",
      })
      .then((res) => {
        const { success, error } = res.body;
        expect(success).equal(false);
        expect(error.name).equal("Wrong current pass");
        done();
      });
  });
  it("Change password with no email found /api/user/password", (done) => {
    request(server)
      .put("/api/user/password")
      .send({
        email: email + "changed2",
        curPass: password,
        newPass: password + "changed",
      })
      .then((res) => {
        const { success, error } = res.body;
        expect(success).equal(false);
        expect(error.name).equal("User not found");
        done();
      });
  });
  it("Change password with undefined newPass /api/user/password", (done) => {
    request(server)
      .put("/api/user/password")
      .send({
        email: email + "changed",
        curPass: password + "changed",
      })
      .then((res) => {
        const { success, error } = res.body;
        expect(success).equal(false);
        expect(error.name).equal("ValidationError");
        expect(error.message).equal(
          "User validation failed: password: Path `password` is required."
        );
        done();
      });
  });
  it("Normal register admin account /api/user/register/admin", (done) => {
    request(server)
      .post("/api/user/register/admin")
      .send({
        name: name + "admin",
        email: email + "admin",
        password: password + "admin",
      })
      .then((res) => {
        const { success, user } = res.body;
        expect(success).equal(true);
        expect(user.role).equal("admin");
        expect(user.name).equal(name + "admin");
        expect(user.email).equal(email + "admin");
        expect(user.password).equal(password + "admin");
        done();
      })
      .catch((err) => done(err));
  });
  it("Register admin account existed email /api/user/register/admin", (done) => {
    request(server)
      .post("/api/user/register/admin")
      .send({
        name: name + "admin",
        email: email + "admin",
        password: password + "admin",
      })
      .then((res) => {
        const { success, error } = res.body;
        expect(success).equal(false);
        expect(error.name).equal("user existed");
        done();
      })
      .catch((err) => done(err));
  });
  it("Normal login admin account /api/user/register/admin", (done) => {
    request(server)
      .post("/api/user/login/admin")
      .send({
        email: email + "admin",
        password: password + "admin",
      })
      .then((res) => {
        const { success, user } = res.body;
        expect(success).equal(true);
        expect(user.email).equal(email + "admin");
        expect(user.password).equal(password + "admin");
        done();
      })
      .catch((err) => done(err));
  });
});

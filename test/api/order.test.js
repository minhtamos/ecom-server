import request from "supertest";
import chai from "chai";
import mocha from "mocha";
import server from "../../server";

const describe = mocha.describe;
const expect = chai.expect;

const anOrder = {
  orderedAt: new Date(),
  name: "name",
  url: "name",
  size: "l",
  color: "red",
  quantity: 3,
  price: 300,
  userEmail: "email@email.com",
  status: "pending",
};

describe("Order API", () => {
  it("POST many orders /api/order/many", (done) => {
    request(server)
      .post("/api/order/many")
      .send([anOrder])
      .then((res) => {
        const { success, orders } = res.body;
        anOrder._id = orders[0]._id;
        expect(success).equal(true);
        expect(orders[0].name).equal(anOrder.name);
        expect(orders[0].userEmail).equal(anOrder.userEmail);
        done();
      });
  });
  it("Table pagination /api/order/pagination/table", (done) => {
    request(server)
      .get("/api/order/pagination/table")
      .then((res) => {
        const { success, orders, limit } = res.body;
        expect(success).equal(true);
        expect(Array.isArray(orders)).equal(true);
        expect(limit).equal(10);
        done();
      });
  });
  it(`Change an order status to completed /api/order/status/completed`, (done) => {
    request(server)
      .put(`/api/order/status/completed?id=${anOrder._id}`)
      .then((res) => {
        const { success, result } = res.body;
        expect(success).equal(true);
        expect(result.nModified).equal(1);
        expect();
        done();
      });
  });
  it(`Change an order status to canceled /api/order/status/canceled`, (done) => {
    request(server)
      .put(`/api/order/status/canceled?id=${anOrder._id}`)
      .then((res) => {
        const { success, result } = res.body;
        expect(success).equal(true);
        expect(result.nModified).equal(1);
        expect();
        done();
      });
  });
  it(`Change an order status to pending /api/order/status/pending`, (done) => {
    request(server)
      .put(`/api/order/status/pending?id=${anOrder._id}`)
      .then((res) => {
        const { success, result } = res.body;
        expect(success).equal(true);
        expect(result.nModified).equal(1);
        expect();
        done();
      });
  });
});

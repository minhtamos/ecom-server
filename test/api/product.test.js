import request from "supertest";
import chai from "chai";
import mocha from "mocha";
import server from "../../server";
import { PageSizeCard, defaultProName, PageSizeTable } from "../../constants";
import { proNameToUrl } from "../../utils";

const describe = mocha.describe;
const expect = chai.expect;

let proID = "";

describe("GET /api/product", () => {
  it("Get all products works", (done) => {
    request(server)
      .get("/api/product")
      .then((res) => {
        const body = res.body;
        expect(Array.isArray(body.products)).equal(true);
        expect(typeof body.success).equal("boolean");
        done();
      })
      .catch((error) => {
        done(error);
      });
  });
});

describe("GET /api/product/pagination/card", () => {
  it("Get all product cards works", (done) => {
    request(server)
      .get("/api/product/pagination/card")
      .then((res) => {
        const body = res.body;
        expect(typeof body.success).equal("boolean");
        expect(Array.isArray(body.products)).equal(true);
        expect(typeof body.total).equal("number");
        expect(typeof body.pages).equal("number");
        expect(typeof body.limit).equal("number");
        expect(body.limit).equal(PageSizeCard);
        done();
      })
      .catch((error) => {
        done(error);
      });
  });
});

describe("GET /api/product/pagination/table", () => {
  it("Get all product cards works", (done) => {
    request(server)
      .get("/api/product/pagination/table")
      .then((res) => {
        const body = res.body;
        expect(typeof body.success).equal("boolean");
        expect(Array.isArray(body.products)).equal(true);
        expect(typeof body.total, "total").equal("number");
        expect(typeof body.pages, "pages").equal("number");
        expect(typeof body.limit, "limit").equal("number");
        expect(body.limit).equal(PageSizeTable);
        done();
      })
      .catch((error) => {
        done(error);
      });
  });
});

describe("POST /api/product", () => {
  it("Post a new product", (done) => {
    request(server)
      .post("/api/product")
      .send({
        name: defaultProName,
        colors: [],
        sizes: ["s"],
        quantities: [2],
        price: 300,
        character: "men",
        brand: "",
        categories: [],
        description: "",
        images: [],
      })
      .then((res) => {
        const { success, product } = res.body;
        expect(typeof success).equal("boolean");
        expect(product).have.property("colors");
        expect(product).have.property("sizes");
        expect(product).have.property("quantities");
        expect(product).have.property("solds");
        expect(product).have.property("images");
        expect(product).have.property("categories");
        expect(product).have.property("reviews");
        expect(product.friendlyUrl).equal(proNameToUrl(defaultProName));
        expect(product.name).equal(defaultProName);
        expect(product.quantities.length).equal(product.solds.length);
        proID = product._id;
        done();
      })
      .catch((error) => {
        done(error);
      });
  });
});

describe("POST /api/product duplicate name", () => {
  it("Post a new product", (done) => {
    request(server)
      .post("/api/product")
      .send({ name: defaultProName })
      .then((res) => {
        const body = res.body;
        expect(body.success).equal(false);
        expect(body.error.name).equal("product existed");
        done();
      })
      .catch((error) => {
        done(error);
      });
  });
});

describe("GET /api/product/others", () => {
  it("Get 8 other products", (done) => {
    request(server)
      .get("/api/product/others")
      .then((res) => {
        const body = res.body;
        expect(typeof body.success).equal("boolean");
        expect(body.products).lengthOf.below(9).above(0);
        done();
      })
      .catch((error) => {
        done(error);
      });
  });
});

describe("GET /api/product/detail/:id", () => {
  it("Get product detail", (done) => {
    request(server)
      .get(`/api/product/detail/${proNameToUrl(defaultProName)}`)
      .then((res) => {
        const body = res.body;
        expect(typeof body.success).equal("boolean");
        expect(body.product).have.property("colors");
        expect(body.product).have.property("sizes");
        expect(body.product).have.property("quantities");
        expect(body.product).have.property("solds");
        expect(body.product).have.property("images");
        expect(body.product).have.property("categories");
        expect(body.product).have.property("reviews");
        expect(body.product.friendlyUrl).equal(proNameToUrl(defaultProName));
        expect(body.product.name).equal(defaultProName);
        done();
      })
      .catch((error) => {
        done(error);
      });
  });
});

describe("POST /api/product/review", () => {
  it("Post a user commennt", (done) => {
    request(server)
      .post(`/api/product/review`)
      .send({
        proName: defaultProName,
        userEmail: "email@email.com",
        userName: "user name",
        reviewedAt: new Date(),
        title: "title",
        cmt: "cmt",
        rating: 5,
      })
      .then((res) => {
        const body = res.body;
        expect(typeof body.success).equal("boolean");
        expect(body.review).have.property("userEmail");
        expect(body.review).have.property("userName");
        expect(body.review).have.property("reviewedAt");
        expect(body.review).have.property("title");
        expect(body.review).have.property("cmt");
        expect(body.review).have.property("rating");
        done();
      })
      .catch((error) => {
        done(error);
      });
  });
});

describe("PUT /api/product/order", () => {
  it("Update product after order", (done) => {
    request(server)
      .put(`/api/product/order`)
      .send({
        sizes: ["s"],
        quantities: [1],
        urls: [proNameToUrl(defaultProName)],
      })
      .then((res) => {
        const { success, products } = res.body;
        expect(typeof success).equal("boolean");
        expect(Array.isArray(products)).equal(true);
        done();
      })
      .catch((error) => {
        done(error);
      });
  });
});

describe("PUT /api/product", () => {
  it("Update product in admin", (done) => {
    request(server)
      .put(`/api/product`)
      .send({
        id: proID,
        name: defaultProName + "changed",
        friendlyUrl: proNameToUrl(defaultProName),
        colors: [],
        sizes: ["s"],
        quantities: [2],
        price: 300,
        character: "men",
        brand: "",
        categories: [],
        description: "",
        images: [],
        solds: [2],
      })
      .then((res) => {
        const { success, product } = res.body;
        expect(typeof success).equal("boolean");
        expect(product.name).equal(defaultProName + "changed");
        done();
      })
      .catch((error) => {
        done(error);
      });
  });
});

describe("DELETE /api/product", () => {
  it("Delete a product", (done) => {
    request(server)
      .delete("/api/product")
      .then((res) => {
        const body = res.body;
        expect(typeof body.success).equal("boolean");
        done();
      })
      .catch((error) => {
        done(error);
      });
  });
});

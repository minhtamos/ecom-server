//import lib
import express from "express";
import cors from "cors";
import bodyParser from "body-parser";
import sgMail from "@sendgrid/mail";
import path from "path";
//config
import { connectDb } from "./config/db";
//import routes
import userRoute from "./routes/user.route";
import productRoute from "./routes/product.route";
import orderRoute from "./routes/order.route";
import categoryRoute from "./routes/category.route";
import imageRoute from "./routes/image.route";
import mailRoute from "./routes/mail.route";
//env
require("dotenv").config();
//app
const app = express();
//mongodb
connectDb(process.env.NODE_ENV);
//middleware
app.use(cors());
// app.use(multer().array());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json({ extended: true }));
//mail
sgMail.setApiKey(process.env.SENDGRID_API_KEY);
//apis routes
const apiRoute = express.Router();
apiRoute.use("/user", userRoute);
apiRoute.use("/product", productRoute);
apiRoute.use("/order", orderRoute);
apiRoute.use("/category", categoryRoute);
apiRoute.use("/image", imageRoute);
apiRoute.use("/mail", mailRoute);
app.use("/api", apiRoute);
app.use("/public", express.static(path.join(__dirname, "public")));
//listen
const port = process.env.PORT || 5000;
app.listen(port, () => console.log(`backend listen at port ${port}`));

export default app;

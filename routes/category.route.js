import express from "express";
import Category from "../models/Category.model";

const categoryRoute = express.Router();

categoryRoute.get("/", (req, res) => {
  const { character } = req.query;
  const queryObj = {};
  if (character && character !== "") {
    queryObj.characters = character;
  }
  
  Category.find(queryObj)
    .then((categories) =>
      res.status(200).json({ success: true, categories: categories })
    )
    .catch((error) => res.status(500).json({ success: false, error: error }));
});

categoryRoute.post("/", async (req, res) => {
  const { name } = req.body;
  try {
    const newCate = await Category.create({ name, characters: [] });
    res.status(200).json({ success: true, category: newCate });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, error: error });
  }
});

categoryRoute.put("/characters", async (req, res) => {
  const { id, characters } = req.body;
  try {
    const newCate = await Category.findOne({ _id: id });
    newCate.characters = characters;
    newCate.markModified("characters");
    await newCate.save();
    res.status(200).json({ success: true, category: newCate });
  } catch (error) {
    console.log(error);
    res.status(500).json({ success: false, error: error });
  }
});

export default categoryRoute;

import express, { query } from "express";
import Product from "../models/product.model";
import { PageSizeCard, PageSizeTable } from "../constants";
import { proNameToUrl } from "../utils";

const productRoute = express.Router();

productRoute.get("/", (req, res) => {
  Product.find()
    .then((products) =>
      res.status(200).json({ success: true, products: products })
    )
    .catch((error) => res.status(500).json({ success: false, error: error }));
});

productRoute.get("/pagination/card", (req, res) => {
  const {
    search,
    page,
    sizes,
    colors,
    brands,
    max,
    min,
    available,
    category,
    character,
    desc,
    asc,
  } = req.query;
  // const query = req.query;

  // switch
  // feild: name  switch-> product:  value

  // sas find (feild : value)
  const queryObj = {};
  let sort = "";
  if (search) {
    queryObj.name = { $regex: search, $options: "i" };
  }
  if (category && category !== "All") {
    queryObj.categories = category;
  }
  if (character) {
    queryObj.character = character;
  }
  if (sizes) {
    queryObj.sizes = { $in: sizes.split(",") };
  }
  if (colors) {
    queryObj.colors = { $in: colors.split(",") };
  }
  if (brands) {
    queryObj.brand = brands.split(",");
  }
  if (max && min) {
    queryObj.price = { $gt: min, $lt: max };
  } else if (max) {
    queryObj.price = { $lt: max };
  } else if (min) {
    queryObj.price = { $gt: min };
  }
  if (available) {
    const outofstock = "Out of stock";
    const instore = "In-store";
    const ops = available.split(",");
    if (ops.length === 1) {
      if (ops[0] === outofstock) {
        queryObj.$expr = { $eq: ["$quantities", "$solds"] };
      } else if (ops[0] === instore) {
        queryObj.$expr = { $ne: ["$quantities", "$solds"] };
      }
    }
  }
  if (asc) {
    const ascArr = asc.split(",");
    for (let i = 0; i < ascArr.length; i++) {
      sort += `${ascArr[i]} `;
    }
  }
  if (desc) {
    const descArr = desc.split(",");
    for (let i = 0; i < descArr.length; i++) {
      sort += `-${descArr[i]} `;
    }
  }
  Product.paginate(queryObj, {
    page: page,
    limit: PageSizeCard,
    select: "name price images quantities friendlyUrl solds",
    sort: sort,
  })
    .then((queryResult) => {
      res.status(200).json({
        success: true,
        products: queryResult.docs,
        total: queryResult.totalDocs,
        pages: queryResult.totalPages,
        limit: queryResult.limit,
      });
    })
    .catch((error) => res.status(500).json({ success: false, error: error }));
});

productRoute.get("/pagination/table", (req, res) => {
  const { page, search } = req.query;
  const queryObj = {};
  if (search && search !== "") {
    queryObj.name = { $regex: search, $options: "i" };
  }

  Product.paginate(queryObj, {
    page: page,
    limit: PageSizeTable,
  })
    .then((queryResult) =>
      res.status(200).json({
        success: true,
        products: queryResult.docs,
        total: queryResult.totalDocs,
        pages: queryResult.totalPages,
        limit: queryResult.limit,
      })
    )
    .catch((error) => res.status(500).json({ success: false, error: error }));
});

productRoute.post("/", (req, res) => {
  const {
    name,
    colors,
    sizes,
    quantities,
    price,
    character,
    brand,
    categories,
    description,
    images,
  } = req.body;

  Product.findOne({ name: name })
    .then((product) => {
      if (!product) {
        const solds = [];
        for (let i = 0; i < quantities.length; i++) {
          solds.push(0);
        }
        Product.create({
          name,
          colors,
          sizes,
          quantities,
          solds,
          price,
          brand,
          character,
          description,
          reviews: [],
          images,
          friendlyUrl: proNameToUrl(name),
          categories,
        })
          .then((newPro) =>
            res.status(200).json({ success: true, product: newPro })
          )
          .catch((error) => {
            res.status(500).json({ success: false, error: error });
          });
      } else {
        res
          .status(400)
          .json({ success: false, error: { name: "product existed" } });
      }
    })
    .catch((error) => {
      res.status(500).json({ success: false, error: error });
    });
});

productRoute.get("/detail/:url", (req, res) => {
  const { url } = req.params;
  Product.findOne({ friendlyUrl: url })
    .then((product) => {
      if (!product) {
        res
          .status(500)
          .json({ success: false, error: { name: "product not found" } });
      } else {
        res.status(200).json({ success: true, product: product });
      }
    })
    .catch((error) => res.status(500).json({ success: false, error: error }));
});

// "reviews.userEmail"
productRoute.post("/review", (req, res) => {
  const {
    proName,
    userName,
    userEmail,
    reviewedAt,
    title,
    cmt,
    rating,
  } = req.body;
  Product.findOne({ name: proName })
    .then((product) => {
      if (product) {
        const reviews = product.reviews;
        let reviewed = false;
        for (const review of reviews) {
          if (review.userEmail === userEmail) {
            reviewed = true;
            break;
          }
        }
        if (reviewed) {
          res.status(400).json({
            success: false,
            error: { name: "You have already reviewed" },
          });
        } else {
          Product.updateOne(
            { name: proName },
            {
              $push: {
                reviews: {
                  proName,
                  userName,
                  userEmail,
                  reviewedAt,
                  title,
                  cmt,
                  rating,
                },
              },
            }
          )
            .then(() =>
              res.status(200).json({
                success: true,
                review: {
                  proName,
                  userName,
                  userEmail,
                  reviewedAt,
                  title,
                  cmt,
                  rating,
                },
              })
            )
            .catch((error) =>
              res.status(500).json({ success: false, error: error })
            );
        }
      } else {
        res.status(400).json({
          success: false,
          error: { name: "Product not found" },
        });
      }
    })
    .catch((error) => res.status(500).json({ success: false, error: error }));
});

productRoute.put("/order", async (req, res) => {
  const { sizes, quantities, urls } = req.body;
  const docs = []; //get documents that need update

  if (sizes.length === quantities.length && sizes.length === urls.length) {
    for (let i = 0; i < urls.length; i++) {
      try {
        const product = await Product.findOne({
          friendlyUrl: urls[i],
        });
        if (!product) {
          res.status(400).json({
            success: false,
            error: { name: "product urls not found", proUrl: urls[i] },
          });
        } else {
          const sizeIndex = product.sizes.indexOf(sizes[i]);
          if (sizeIndex === -1) {
            res.status(400).json({
              success: false,
              error: { name: "size not found", proUrl: urls[i] },
            });
          } else {
            if (
              product.quantities[sizeIndex] -
                product.solds[sizeIndex] -
                quantities[i] <
              0
            ) {
              res.status(400).json({
                success: false,
                error: {
                  name: "Not enough quantity",
                },
                proUrl: urls[i],
                proName: product.name,
                left: product.quantities[sizeIndex] - product.solds[sizeIndex],
              });
              return;
            } else {
              docs.push({ product, sizeIndex: sizeIndex });
            }
          }
        }
      } catch (error) {
        res.json({
          success: false,
          error: error,
          proUrl: urls[i],
        });
      }
    }
  } else {
    res.status(400).json({
      success: false,
      error: { name: "arrays not same length" },
    });
  }
  for (let i = 0; i < docs.length; i++) {
    const { product, sizeIndex } = docs[i];
    product.solds[sizeIndex] += quantities[i];
    product.markModified("solds");
    try {
      await product.save();
    } catch (error) {
      res.json({
        success: false,
        error: error,
        proUrl: urls[i],
      });
    }
  }
  res.status(200).json({ success: true, products: docs });
});

productRoute.get("/others", (req, res) => {
  Product.find({}, null, { limit: 8 })
    .select("name images friendlyUrl")
    .then((products) =>
      res.status(200).json({ success: true, products: products })
    )
    .catch((error) => res.status(500).json({ success: false, error: error }));
});

productRoute.delete("/", (req, res) => {
  const { url } = req.query;

  Product.deleteOne({ friendlyUrl: url })
    .then(() => res.status(200).json({ success: true }))
    .catch((error) => res.status(500).json({ success: false, error: error }));
});

productRoute.put("/", (req, res) => {
  const {
    id,
    name,
    colors,
    sizes,
    quantities,
    price,
    character,
    description,
    images,
    friendlyUrl,
    categories,
  } = req.body;
  Product.findOne({ _id: id })
    .then((product) => {
      if (!product) {
        res
          .status(500)
          .json({ success: false, error: { name: "product not found" } });
      } else {
        product.name = name;
        product.colors = colors;
        product.markModified("colors");
        product.sizes = sizes;
        product.markModified("sizes");
        product.quantities = quantities;
        product.markModified("quantities");
        product.price = price;
        product.markModified("price");
        product.character = character;
        product.markModified("character");
        product.description = description;
        product.markModified("description");
        product.images = images;
        product.markModified("images");
        product.categories = categories;
        product.markModified("categories");
        product.friendlyUrl = friendlyUrl;
        product.markModified("friendlyUrl");
        let newSolds = [];
        for (let i = 0; i < sizes.length - product.sizes.length; i++) {
          newSolds.push(0);
        }
        product.solds = [...product.solds, ...newSolds];
        product.markModified("solds");
        const sizeIndex = product.save().then(() => {
          res.status(200).json({ success: true, product: product });
        });
      }
    })
    .catch((error) => res.status(500).json({ success: false, error: error }));
});

productRoute.put("/order/cancel", (req, res) => {
  const { url, size, quantity } = req.body;
  Product.findOne({ friendlyUrl: url })
    .then((product) => {
      if (!product) {
        res
          .status(500)
          .json({ success: false, error: { name: "product not found" } });
      } else {
        const sizeIndex = product.sizes.indexOf(size);
        product.solds[sizeIndex] -= quantity;
        product.markModified("solds");
        product
          .save()
          .then(() => {
            res.status(200).json({ success: true, product: product });
          })
          .catch((error) =>
            res.status(500).json({ success: false, error: error })
          );
      }
    })
    .catch((error) => res.status(500).json({ success: false, error: error }));
});

export default productRoute;

import express from "express";
import User from "../models/user.model";

const userRoute = express.Router();

//user register
userRoute.post("/register", (req, res) => {
  const { email, name, password } = req.body;
  User.findOne({ email })
    .then((user) => {
      if (!user) {
        User.create({ email, name, password, role: "user" })
          .then((user) => res.status(200).json({ success: true, user: user }))
          .catch((error) =>
            res.status(500).json({ success: false, error: error })
          );
      } else {
        res
          .status(500)
          .json({ success: false, error: { name: "user existed" } });
      }
    })
    .catch((error) => res.status(406).json({ success: false, error: error }));
});

//user register admin account
userRoute.post("/register/admin", (req, res) => {
  const { email, name, password } = req.body;
  User.findOne({ email })
    .then((user) => {
      if (!user) {
        User.create({ email, name, password, role: "admin" })
          .then((user) => res.status(200).json({ success: true, user: user }))
          .catch((error) =>
            res.status(500).json({ success: false, error: error })
          );
      } else {
        res
          .status(500)
          .json({ success: false, error: { name: "user existed" } });
      }
    })
    .catch((error) => res.status(406).json({ success: false, error: error }));
});

//user login
userRoute.post("/login", (req, res) => {
  const { email, password } = req.body;
  User.findOne({ email, password })
    .then((user) => {
      if (!user) {
        res
          .status(406)
          .json({ success: false, error: { name: "wrong email or password" } });
      } else {
        res.status(200).json({ success: true, user: user });
      }
    })
    .catch((error) => res.status(409).json({ success: false, error: error }));
});

//update user info
userRoute.put("/info", (req, res) => {
  const { email, name, oldEmail } = req.body;
  User.findOne({ email: oldEmail }).then((user) => {
    if (!user) {
      res
        .status(400)
        .json({ success: false, error: { name: "User not found" } });
    } else {
      user.email = email;
      user.name = name;
      user
        .save()
        .then(() => res.status(200).json({ success: true, user: user }))
        .catch((error) =>
          res.status(500).json({ success: false, error: error })
        );
    }
  });
});

//update user info
userRoute.put("/password", (req, res) => {
  const { curPass, newPass, email } = req.body;
  User.findOne({ email }).then((user) => {
    if (!user) {
      res
        .status(400)
        .json({ success: false, error: { name: "User not found" } });
    } else {
      if (user.password === curPass) {
        user.password = newPass;
        user
          .save()
          .then(() => res.status(200).json({ success: true, user: user }))
          .catch((error) =>
            res.status(500).json({ success: false, error: error })
          );
      } else {
        res
          .status(400)
          .json({ success: false, error: { name: "Wrong current pass" } });
      }
    }
  });
});

//user login
userRoute.post("/login/admin", (req, res) => {
  const { email, password } = req.body;
  User.findOne({ email, password, role: "admin" })
    .then((user) => {
      if (!user) {
        res
          .status(406)
          .json({ success: false, error: { name: "Wrong email or password" } });
      } else {
        res.status(200).json({ success: true, user: user });
      }
    })
    .catch((error) => res.status(409).json({ success: false, error: error }));
});

export default userRoute;

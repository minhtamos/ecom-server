import express from "express";
import Order from "../models/order.model";
import sgMail from "@sendgrid/mail";
import { PageSizeTable } from "../constants";
import mongoose from "mongoose";

const orderRoute = express.Router();

//admin email: tam310599@gmail.com
//shop email: minhtamoshop@gmail.com
const sendEmailOrder = (orderID, userEmail, status) => {
  const msg = {
    to: userEmail ? userEmail : "tam310599@gmail.com", //if not passing userEmail then send to admin email
    from: "minhtamoshop@gmail.com", //shop email
    subject: "Order announcement",
    text:
      status === "pending"
        ? `You just ordered from minhtamoshop`
        : `Order id: ${orderID} just canceled`,
  };
  sgMail
    .send(msg)
    .then(() => {
      console.log("Email sent");
    })
    .catch((error) => {
      console.log(error.response.body);
      // console.log(error.response.body.errors[0].message)
    });
};

orderRoute.get("/", (req, res) => {
  const { userEmail } = req.query;
  const queryObj = {};
  if (userEmail) {
    queryObj.userEmail = userEmail;
  }
  Order.find(queryObj)
    .then((orders) => {
      res.status(200).json({ success: true, orders: orders });
    })
    .catch((error) => {
      res.status(500).json({ success: false, error: error });
    });
});

orderRoute.post("/many", (req, res) => {
  const newOrders = req.body;
  Order.insertMany(newOrders)
    .then((orders) => {
      res.status(200).json({ success: true, orders: orders });
    })
    .catch((error) => {
      res.status(500).json({ success: false, error: error });
    });
  sendEmailOrder("", newOrders[0].userEmail, "pending");
});

orderRoute.get("/pagination/table", async (req, res) => {
  const { page, search, sortby } = req.query;
  if (search && search !== "") {
    try {
      const order = await Order.findById(search);
      res.status(200).json({
        success: true,
        orders: order ? [order] : [],
      });
    } catch (error) {
      res.status(500).json({ success: false, error: error });
    }
  } else {
    let sortStr = "";
    if (sortby && sortby !== "") {
      const [sortDir, sortOp] = sortby.split("-");
      if (sortDir === "asc") {
        sortStr = sortOp;
      }
      if (sortDir === "desc") {
        sortStr = "-" + sortOp;
      }
    }
    Order.paginate(
      {},
      {
        page: page,
        limit: PageSizeTable,
        sort: sortStr,
      }
    )
      .then((queryResult) =>
        res.status(200).json({
          success: true,
          orders: queryResult.docs,
          total: queryResult.totalDocs,
          pages: queryResult.totalPages,
          limit: queryResult.limit,
        })
      )
      .catch((error) => res.status(500).json({ success: false, error: error }));
  }
});

orderRoute.put("/status/completed", (req, res) => {
  const { id } = req.query;

  Order.updateOne(
    { _id: id },
    {
      status: "completed",
    }
  )
    .then((result) => res.status(200).json({ success: true, result: result }))
    .catch((error) => res.status(500).json({ success: false, error: error }));
});

orderRoute.put("/status/canceled", (req, res) => {
  const { id } = req.query;

  Order.updateOne(
    { _id: id },
    {
      status: "canceled",
    }
  )
    .then((result) => {
      sendEmailOrder(id, "", "canceled");
      res.status(200).json({ success: true, result: result });
    })
    .catch((error) => res.status(500).json({ success: false, error: error }));
});

orderRoute.put("/status/pending", (req, res) => {
  const { id } = req.query;

  Order.updateOne(
    { _id: id },
    {
      status: "pending",
    }
  )
    .then((result) => res.status(200).json({ success: true, result: result }))
    .catch((error) => res.status(500).json({ success: false, error: error }));
});

orderRoute.delete("/", (req, res) => {
  const { id } = req.query;

  Order.deleteOne({ _id: id })
    .then(() => res.status(200).json({ success: true }))
    .catch((error) => res.status(500).json({ success: false, error: error }));
});

export default orderRoute;

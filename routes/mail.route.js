import express from "express";
import sgMail from "@sendgrid/mail";

const imageRoute = express.Router();

imageRoute.get("/send", (req, res) => {
  const msg = {
    to: "tam310599@gmail.com",
    from: "minhtamoshop@gmail.com",
    subject: "Confirm your order",
    text: "One more step to complete your order",
    html: "<div><h1>Confirm mail</h1><a href='http://localhost:3000'>Confirm order</a></div>",
  };
  sgMail
    .send(msg)
    .then(() => {
      console.log("Message sent");
    })
    .catch((error) => {
      console.log(error.response.body);
      // console.log(error.response.body.errors[0].message)
    });
  res.send("sent");
});

export default imageRoute;

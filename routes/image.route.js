import express from "express";
import multer from "multer";
import path from "path";
import fs from "fs";
import { uuid } from 'uuidv4';

const imageRoute = express.Router();

// Set The Storage Engine
const storage = multer.diskStorage({
  destination: `./public/images`,
  filename: function (req, file, cb) {
    cb(
      null,
      uuid() + path.extname(file.originalname)
    );
  },
});

// Init Upload
const upload = multer({
  storage: storage,
  limits: { fileSize: 1000000 },
  fileFilter: function (req, file, cb) {
    checkFileType(file, cb);
  },
}).single("image");

// Check File Type
function checkFileType(file, cb) {
  // Allowed ext
  const filetypes = /jpeg|jpg|png|gif/;
  // Check ext
  const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
  // Check mime
  const mimetype = filetypes.test(file.mimetype);

  if (mimetype && extname) {
    return cb(null, true);
  } else {
    cb("Error: Images Only!");
  }
}

imageRoute.post("/", (req, res) => {
  upload(req, res, (error) => {
    if (error) {
      res.json({ success: false, error: error });
    } else {
      if (req.file == undefined) {
        res.json({ success: false, error: { name: "no file found!" } });
      } else {
        res.json({
          success: true,
          url: `/public/images/${req.file.filename}`,
        });
      }
    }
  });
});

imageRoute.delete("/", (req, res) => {
  const name = req.query.name;
  fs.unlink(path.resolve(`${__dirname}/../public/images/${name}`), (error) => {
    if (error) {
      res.status(500).json({ success: false, error: error });
    } else {
      res.status(200).json({ success: true, name: name });
    }
  });
});

export default imageRoute;

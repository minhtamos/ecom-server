import mongoose from "mongoose";
import mongoosePaginate from "mongoose-paginate-v2";

const Schema = mongoose.Schema;

const orderSchema = new Schema(
  {
    orderedAt: Date,
    name: String,
    url: String,
    size: String,
    color: String,
    quantity: Number,
    price: Number,
    userEmail: String,
    status: String, //completed, pending, canceled
  },
  {
    timestamps: true,
  }
);

orderSchema.plugin(mongoosePaginate);

const Order = mongoose.model("Order", orderSchema);
export default Order;

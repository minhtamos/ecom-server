import mongoose from "mongoose";
const Schema = mongoose.Schema;

const categorySchema = new Schema(
  {
    name: {
      type: String,
      unique: [true, "Must have name"],
      required: true,
    },
    characters: [String],
  },
  { timestamps: true }
);

const Category = mongoose.model("Category", categorySchema);
export default Category;

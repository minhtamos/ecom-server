import mongoose from "mongoose";
import mongoosePaginate from "mongoose-paginate-v2";

const Schema = mongoose.Schema;

const productSchema = new Schema(
  {
    name: {
      type: String,
      unique: true,
    },
    colors: [String],
    sizes: [String],
    quantities: [Number],
    solds: [Number],
    price: Number,
    character: String,
    brand: String,
    description: String,
    reviews: [
      {
        userName: String,
        userEmail: String,
        reviewedAt: Date,
        title: String,
        cmt: String,
        rating: Number,
      },
    ],
    images: [String],
    friendlyUrl: String,
    character: String, //men, ladies, girls, boys
    categories: [String],
  },
  { timestamps: true }
);

productSchema.plugin(mongoosePaginate);

const Product = mongoose.model("Product", productSchema);
export default Product;

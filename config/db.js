import mongoose from "mongoose";
import { MongoMemoryServer } from "mongodb-memory-server";

const mongod = new MongoMemoryServer();

function connectDb(mode) {
  return new Promise(async (resolve, reject) => {
    if (mode === "test") {
      const uri = await mongod.getConnectionString();
      const mongooseOpts = {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,
      };
      try {
        await mongoose.connect(uri, mongooseOpts);
        resolve();
      } catch (error) {
        reject(error);
      }
    } else {
      mongoose
        .connect(process.env.mongodb_connection, {
          useNewUrlParser: true,
          useUnifiedTopology: true,
          useCreateIndex: true,
        })
        .then((res, err) => {
          if (err) return reject(err);
          resolve();
        })
        .catch((err) => {
          if (err) return reject(err);
          resolve();
        });
    }
  });
}

function closeDb() {
  return new Promise(async (resolve, reject) => {
    try {
      await mongoose.connection.close();
      await mongod.stop();
      resolve();
    } catch (error) {
      reject(error);
    }
  });
}

export { connectDb, closeDb };
